import React, { useState, useEffect } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import Link from "next/link";

const Menu = ({ Children }) => {
	const [users, setUsers] = useState([]);
	const [items, setItems] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/details", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setUsers(response.success);
				fetch("http://localhost:8000/api/menu/adminlist", {
					method: "POST",
					body: JSON.stringify({
						admin_id: response.success.id
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
						Accept: "application/json",
						Authorization: `Bearer ${token}`,
					},
				})
					.then(function (response) {
						return response.json();
					})
					.then(function (response) {
						setItems(response)
					});
			});
	
	}, []);

	const { SearchBar } = Search;

	function rankFormatter(cell, row, rowIndex, formatExtraData, dataField) {
		const onSubmitRemove = (id) => {
			const token = localStorage.usertoken;
			fetch("http://localhost:8000/api/menu/item/destroy", {
				method: "DELETE",
				body: JSON.stringify({
					id: id
				}),
				headers: {
					"Content-type": "application/json; charset=UTF-8",
					Accept: "application/json",
					Authorization: `Bearer ${token}`,
				},
			});
			Notiflix.Notify.Success("Item Eliminado");
			const interval = setInterval(() => {
				window.location.reload(false);
			}, 2000);
			return () => clearInterval(interval);
		};

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				<i onClick={() => onSubmitView(row.id)} className="icon feather icon-eye f-w-600 f-16 m-r-15 text-c-green"></i>
				<i
					onClick={() => onSubmitRemove(row.id)}
					className="feather icon-trash-2 f-w-600 f-16 text-c-red"
				></i>
			</div>
		);
	}
	function imageurl(cell, row, rowIndex, formatExtraData, dataField) {

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				<img height="50" src={row.imageurl} style={{borderRadius: 10}}/>
		
			</div>
		);
	}
	const onSubmitView = (id) => {
		event.preventDefault();
		const token = localStorage.usertoken;
		fetch("http://localhost:3333/api/v1/admin/categories/" + id, {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				return setItems(response);
			});

		handleClick();
	};

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Listado de Menu</title>
			</Head>
			<div className="row">
				<div className="col-xl-12 col-md-12">
					<div className="card table-card loading">
						<div className="card-header">
							<h5>Menú</h5>
							<div className="card-header-right">
								<div className="btn-group card-option">
									<Link href="/menu/create">
										<a className="btn btn-md btn-dark">Crear Item</a>
									</Link>
								</div>
							</div>
						</div>
						<div className="card-body p-0">
							<div className="table-responsive-sm table-responsive-xs table-responsive-md table-responsive-lg">
								<>
									<ToolkitProvider
										data={items}
										keyField="name"
										columns={[
											{
												dataField: "imageurl",
												text: "Imagen",
												sort: false,
												formatter: imageurl,
											},
											{
												dataField: "name",
												text: "Nombre",
												sort: false,
											},
											{
												dataField: "description",
												text: "Descripción",
												sort: false,
											},
											{
												dataField: "ingredients",
												text: "Ingredientes",
												sort: false
											},
											{
												dataField: "price",
												text: "Precio",
												sort: false,
											},
											{
												dataField: "currency",
												text: "Moneda",
												sort: false,
											},
											{
												dataField: "getpromo",
												text: "Promo",
												sort: false,
											},
											{
												dataField: "type",
												text: "Tipo",
												sort: false,
											},
											{
												dataField: "status",
												text: "Status",
												sort: false,
											},
											{
												dataField: "edit",
												text: "Acción",
												sort: false,
												formatter: rankFormatter,
												headerAttrs: { width: 50 },
												attrs: {
													width: 100,
													className: "EditRow",
												},
											},
										]}
										search
									>
										{(props) => (
											<div className="py-4">
												<div
													id="datatable-basic_filter"
													className="dataTables_filter px-4 pb-1"
												>
													<label>
														Buscar:
														<SearchBar
															className="form-control-sm"
															placeholder=""
															{...props.searchProps}
														/>
													</label>
												</div>
												<BootstrapTable
													{...props.baseProps}
													pagination={paginationFactory()}
													bordered={false}
												/>
											</div>
										)}
									</ToolkitProvider>
								</>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Page>
	);
};

export default Menu;
