import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import { getUsers } from "../../services/users.js";
import { getLocals } from "../../services/locals.js";
import {
	addPin,
  } from "../../services/pin.js";

const Create = ({ children }) => {
	const [users, setUsers] = useState([]);
	const [locals, setLocals] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		number: {},
		touched: {},
		errors: {}
	});

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value
			},
		}));
	};

	useEffect(() => {
		getUsers().then((response) => {
			setUsers(response.success);
			let setUserId = (response.success.id);
			getLocals(setUserId).then((response) => {
			setLocals(response.success);
			let list = (response.success);
			  console.log(list, 'esto es list')
		});
	});
	}, []);

	const onSubmitPin = (event) => {
		event.preventDefault();
		// notiflix();
		const alphabet=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

		let ranletter1 = alphabet[Math.floor(Math.random() * alphabet.length)];
		let ranletter2 = alphabet[Math.floor(Math.random() * alphabet.length)];
		let ranNum = Math.floor((Math.random() * 99999999999) * 7);
		let ranCode = ranletter1 + ranletter2 + ranNum;
		let local_id = formState.values.local_id;
		let tbl_number = formState.values.tbl_number;
		let status = formState.values.status;
		let id = users.id;

		addPin(ranCode, id, local_id, tbl_number, status).then((response) => {
			Notiflix.Notify.Success("Pin Creado Exitosamente");
		});
	};

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Crear Pin</title>
			</Head>
			<form onSubmit={onSubmitPin}>
			<div className="row">
				<div className="col-xl-4 col-md-12"></div>
				<div className="col-xl-4 col-md-12">
					<div className="card">
						<div className="card-body">
							<div className="bt-wizard" id="besicwizard">
								<div className="tab-content">
									<div className="tab-pane show active" id="b-w-tab1">
									
										<div className="form-group row">
										{users.status === '1' && (
												<div className="form-group row">
												<label htmlFor="b-t-pwd" className="col-sm-12 col-form-label">
													Local
												</label>
												<div className="col-sm-12">
													<select className="form-control"
														name="local_id"
														onChange={handleChange}>
														<option value={formState.values.local_id = "1"}>1</option>
														<option value={formState.values.local_id = "2"}>2</option>
													</select>
												</div>
											</div>
											)}
												<label htmlFor="b-t-name" className="col-sm-12 col-form-label">
													Número de la Mesa
												</label>
												<div className="col-sm-12">
													<input
														type="number"
														className="form-control"
														name="tbl_number"
														onChange={handleChange}
														value={formState.values.tbl_number}
													/>
												</div>
											</div>
											<div className="form-group row">
												<label htmlFor="b-t-pwd" className="col-sm-3 col-form-label">
													Status
												</label>
												<div className="col-sm-9">
													<select className="form-control"
														name="status"
														onChange={handleChange}>
														<option value={formState.values.status = "1"}>Activo</option>
														<option value={formState.values.status = "2"}>Inactivo</option>
													</select>
												</div>
											</div>
											<button type="submit" className="btn btn-primary btn-block">
												Generar Pin
											</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div className="col-xl-4 col-md-12"></div>
		
			</div>
			</form>
		</Page>
	);
};

export default Create;
