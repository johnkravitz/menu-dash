import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";

const Reports = ({ Children }) => {
	const [posts, setPosts] = useState([]);
	const [items, setItems] = useState({});
	const [open, setOpen] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	const handleClickAdd = () => {
		setOpen(!open);
	};

	const handleClick = () => {
		setOpen(!open);
	};

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	useEffect(() => {
		notiflix();
		const token = localStorage.usertoken;
		fetch("http://localhost:3333/api/v1/admin/Reports", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setPosts(response);
			});
	}, []);

	const { SearchBar } = Search;

	function rankFormatter(cell, row, rowIndex, formatExtraData, dataField) {
		const onSubmitRemove = (id) => {
			const token = localStorage.usertoken;
			fetch("http://localhost:3333/api/v1/admin/Reports/" + id, {
				method: "DELETE",
				headers: {
					"Content-type": "application/json; charset=UTF-8",
					Accept: "application/json",
					Authorization: `Bearer ${token}`,
				},
			});
			Notiflix.Notify.Success("Línea Eliminada");
			const interval = setInterval(() => {
				window.location.reload(false);
			}, 2000);
			return () => clearInterval(interval);
		};

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				<i onClick={() => onSubmitView(row.id)} className="far fa-edit"></i>
				<i
					onClick={() => onSubmitRemove(row.id)}
					className="fas fa-trash-alt ml-3"
				></i>
			</div>
		);
	}

	const onSubmitView = (id) => {
		event.preventDefault();
		const token = localStorage.usertoken;
		fetch("http://localhost:3333/api/v1/admin/Reports/" + id, {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				return setItems(response);
			});

		handleClick();
	};

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Listado de Lineas</title>
			</Head>
			<div class="row">
				<div class="col-lg-9 col-md-8">
					<div class="col-sm-12 card">
						<div class="card-block">
							<div class="row">
								<div class="col-xs-12 col-sm-5">
									<div class="form-group">
										<label for="exampleSelect1" class="form-control-label">
											Desde
										</label>
										<select class="form-control" id="exampleSelect1">
											<option>Seleccionar</option>
											<option>Tarjeta</option>
											<option>Giros</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-5">
									<div class="form-group">
										<label for="exampleSelect1" class="form-control-label">
											hasta
										</label>
										<select class="form-control" id="exampleSelect1">
											<option>Seleccionar</option>
											<option>Tarjeta</option>
											<option>Giros</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-2">
									<button
										type="submit"
										class="btn btn-success btn-block  waves-effect waves-light mt-4"
									>
										REPORTE
									</button>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-12 card">
						<div class="card-block">
							<h6 class="m-b-20">Reporte Detallado</h6>

							<div class="table-responsive">
								<table class="table m-b-0 photo-table">
									<thead>
										<tr class="text-uppercase">
											<th>Fecha</th>
											<th>Venta</th>
											<th>Comisiones</th>
											<th>Total Neto</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>24/04/2020</td>
											<td>15.000.000Gs</td>
											<td>5.000.000Gs</td>
											<td>
												<label class="text-success">10.000.000Gs</label>
											</td>
										</tr>
										<tr>
											<td>23/04/2020</td>
											<td>15.000.000Gs</td>
											<td>5.000.000Gs</td>
											<td>
												<label class="text-success">10.000.000Gs</label>
											</td>
										</tr>
										<tr>
											<td>22/04/2020</td>
											<td>15.000.000Gs</td>
											<td>5.000.000Gs</td>
											<td>
												<label class="text-success">10.000.000Gs</label>
											</td>
										</tr>
										<tr>
											<td>21/04/2020</td>
											<td>15.000.000Gs</td>
											<td>5.000.000Gs</td>
											<td>
												<label class="text-success">10.000.000Gs</label>
											</td>
										</tr>
										<tr>
											<td>20/04/2020</td>
											<td>15.000.000Gs</td>
											<td>5.000.000Gs</td>
											<td>
												<label class="text-success">10.000.000Gs</label>
											</td>
										</tr>
										<tr>
											<td>24/04/2020</td>
											<td>15.000.000Gs</td>
											<td>5.000.000Gs</td>
											<td>
												<label class="text-success">10.000.000Gs</label>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-md-12">
					<div class="col-xl-12 col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="row align-items-center">
									<div class="col-7">
										<h3>16,756Gs</h3>
										<h6 class="text-muted m-b-0">
											Total Mes
											<i class="fa fa-caret-down text-c-red m-l-10"></i>
										</h6>
									</div>
									<div class="col-5">
										<div id="seo-chart1" class="d-flex align-items-end"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-12 col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="row align-items-center">
									<div class="col-10">
										<h3>49.545.215Gs</h3>
										<h6 class="text-muted m-b-0">
											Total Anual
											<i class="fa fa-caret-up text-c-green m-l-10"></i>
										</h6>
									</div>
									<div class="col-2">
										<div id="seo-chart2" class="d-flex align-items-end"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Page>
	);
};

export default Reports;
