import Head from "next/head";
import React, { useEffect } from "react";
import Notiflix from "notiflix";
const Default = ({ children, title = "Default" }) => {
	useEffect(() => {
		Notiflix.Loading.Init({
			className: "notiflix-loading",
			zindex: 5000,
			backgroundColor: "#ff3a00",
			rtl: false,
			useGoogleFont: true,
			fontFamily: "Quicksand",
			cssAnimation: true,
			cssAnimationDuration: 0,
			clickToClose: false,
			customSvgUrl: null,
			svgSize: "80px",
			svgColor: "#ffffff",
			messageID: "NotiflixLoadingMessage",
			messageFontSize: "15px",
			messageMaxLength: 34,
			messageColor: "#dcdcdc",
		});
		Notiflix.Loading.Dots();
		Notiflix.Loading.Remove(1000);
	}, []);
	return (
		<div>
			<Head>
				<title>Menufy</title>

				<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
				<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

				<meta charset="utf-8" />
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"
				/>
				<meta http-equiv="X-UA-Compatible" content="IE=edge" />
				<meta name="description" content="" />
				<meta name="keywords" content="" />
				<meta name="author" content="Phoenixcoded" />

				<link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />

				<link rel="stylesheet" href="assets/css/style.css" />
			</Head>

			<div class="auth-wrapper">{children}</div>

			<script src="assets/js/vendor-all.min.js"></script>
			<script src="assets/js/plugins/bootstrap.min.js"></script>
			<script src="assets/js/ripple.js"></script>
			<script src="assets/js/pcoded.min.js"></script>
		</div>
	);
};

export default Default;
