import React, { useState } from "react";
import Notiflix from "notiflix";

const Edit = (props) => {
	const { items } = props;
	const [open, setOpen] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	const onSubmitEdit = (id) => {
		event.preventDefault();
		const token = localStorage.usertoken;
		fetch("http://localhost:3333/api/v1/admin/lines/" + id, {
			method: "PATCH",
			body: JSON.stringify({
				name: formState.values.name,
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				if (response[0]) {
					Notiflix.Notify.Failure(response[0].message);
				} else {
					Notiflix.Notify.Success("Categoria Actualizada");
					const interval = setInterval(() => {
						window.location.reload(false);
					}, 2000);
					return () => clearInterval(interval);
				}
			});
	};
	const handleClickAdd = () => {
		setOpen(!open);
	};

	const handleClick = () => {
		setOpen(open);
	};

	return (
		<div class="card-body">
			<form onSubmit={() => onSubmitEdit(items.id)}>
				<div className="form-row">
					<div className="col-md-12 mb-3">
						<label className="form-control-label">Nombre</label>
						<input
							type="text"
							className="form-control"
							name="name"
							onChange={handleChange}
							value={formState.values.name ? formState.values.name : items.name}
							required
						/>
						<div className="valid-feedback">Escriba un Nombre</div>
					</div>
				</div>
				<button type="submit" class="btn btn-primary btn-block">
					Guardar
				</button>
			</form>
		</div>
	);
};

export default Edit;
