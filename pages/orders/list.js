import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import Link from "next/link";

const List = ({ url: { query: { id } } })  => {
  const [orders, setOrders] = useState([]);
  const [users, setUsers] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

  useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/details", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setUsers(response.success);
				fetch("http://localhost:8000/api/order/adminlist", {
					method: "POST",
					body: JSON.stringify({
						admin_id: response.success.id
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
						Accept: "application/json",
						Authorization: `Bearer ${token}`,
					},
				})
					.then(function (response) {
						return response.json();
					})
					.then(function (response) {
						if(response){
						setOrders(response);
						}else{
							setOrders([])
						}
					});
			});
	
	}, []);


	const { SearchBar } = Search;

	function rankFormatter(cell, row, rowIndex, formatExtraData, dataField) {
		const onSubmitRemove = (iddelete) => {
			const token = localStorage.usertoken;
			fetch("http://localhost:8000/api/order/delete", {
				method: "post",
				body: JSON.stringify({
					id: iddelete
				}),
				headers: {
					"Content-type": "application/json; charset=UTF-8",
					Accept: "application/json",
					Authorization: `Bearer ${token}`,
				},
			});
			Notiflix.Notify.Success("Orden Eliminada");
			// const interval = setInterval(() => {
			// 	window.location.reload(false);
			// }, 2000);
			// return () => clearInterval(interval);
		};

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				{/* <i onClick={() => onSubmitView(row.id)} className="far fa-edit"></i> */}
        <Link href={{ pathname: '/orders', query: { id: row.tbl_pin }}} ><a>
					<i className="icon feather icon-eye f-w-600 f-16 m-r-15 text-c-green"></i></a></Link>
				<i
					onClick={() => onSubmitRemove(row.id)}
					className="feather icon-trash-2 f-w-600 f-16 text-c-red"
				></i>
			</div>
		);
	}
	function imageurl(cell, row, rowIndex, formatExtraData, dataField) {

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				<img height="50" src={row.imageurl} style={{borderRadius: 10}}/>
		
			</div>
		);
	}

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Listado de Ordenes</title>
			</Head>
			<div className="row">
			<div className="col-xl-12 col-md-12">
					<div className="card table-card loading">
						<div className="card-header">
							<h5>Ordenes</h5>
						</div>
						<div className="card-body p-0">
							<div className="table-responsive-sm table-responsive-xs table-responsive-md">
								<>
									<ToolkitProvider
										data={orders}
										keyField="name"
										columns={[
											{
												dataField: "id",
												text: "ID",
												sort: false
											},
											{
												dataField: "tbl_pin",
												text: "Pin de mesa",
												sort: false,
											},
											{
												dataField: "tbl_number",
												text: "Nº de Mesa",
												sort: false,
                      },
                      
											{
												dataField: "order_items",
												text: "Nº de Items",
												sort: false,
											},
											{
												dataField: "total_amount",
												text: "Total a Pagar",
												sort: false,
											},
											{
												dataField: "edit",
												text: "Acción",
												sort: false,
												formatter: rankFormatter,
												headerAttrs: { width: 50 },
												attrs: {
													width: 100,
													className: "EditRow",
												},
											},
										]}
										search
									>
										{(props) => (
											<div className="py-4">
												<div
													id="datatable-basic_filter"
													className="dataTables_filter px-4 pb-1"
												>
													<label>
														Buscar:
														<SearchBar
															className="form-control-sm"
															placeholder=""
															{...props.searchProps}
														/>
													</label>
												</div>
												<BootstrapTable
													{...props.baseProps}
													pagination={paginationFactory()}
													bordered={false}
												/>
											</div>
										)}
									</ToolkitProvider>
								</>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Page>
	);
};

export default List;
