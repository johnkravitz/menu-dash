import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import EditForm from "./edit.js";

const Create = ({ children }) => {
	const [posts, setPosts] = useState([]);
	const [items, setItems] = useState({});
	const [open, setOpen] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	// const handleClickAdd = () => {
	// 	setOpen(!open);
	// };

	// const handleClick = () => {
	// 	setOpen(!open);
	// };

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	useEffect(() => {
		notiflix();
		const token = localStorage.usertoken;
		fetch("http://localhost:3333/api/v1/admin/subcategories", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setPosts(response);
			});
	}, []);

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Crear Producto</title>
			</Head>
			<div class="row">
				<div class="col-xl-3 col-md-12">
					<div class="card latest-update-card">
						<div class="card-header">
							<h5>Subir Fotográfia</h5>
						</div>
						<EditForm items={items} />
					</div>
				</div>
				<div class="col-xl-6 col-md-12">
					<div class="card">
						<div class="card-header">
							<h5>Datos del Cliente</h5>
						</div>
						<div class="card-body">
							<div class="bt-wizard" id="besicwizard">
								<div class="tab-content">
									<div class="tab-pane show active" id="b-w-tab1">
										<div class="row">
											<div class="col-xs-12 col-lg-6">
												<div class="form-group">
													<label
														for="exampleInputEmail"
														class="form-control-label"
													>
														Nombre
													</label>
													<input
														type="email"
														class="form-control"
														id="exampleInputEmail"
														placeholder="John"
													/>
												</div>
											</div>
											<div class="col-xs-12 col-lg-6">
												<div class="form-group">
													<label
														for="exampleInputEmail"
														class="form-control-label"
													>
														Apellido
													</label>
													<input
														type="email"
														class="form-control"
														id="exampleInputEmail"
														placeholder="Doe"
													/>
												</div>
											</div>
											<div class="col-xs-12 col-lg-6">
												<div class="form-group">
													<label
														for="exampleInputEmail"
														class="form-control-label"
													>
														CI/RUC
													</label>
													<input
														type="email"
														class="form-control"
														id="exampleInputEmail"
														placeholder="8524898-5"
													/>
												</div>
											</div>
											<div class="col-xs-12 col-lg-6">
												<div class="form-group">
													<label
														for="exampleInputEmail"
														class="form-control-label"
													>
														Sexo
													</label>
													<input
														type="email"
														class="form-control"
														id="exampleInputEmail"
														placeholder="Masculino"
													/>
												</div>
											</div>
											<div class="col-xs-12 col-lg-6">
												<div class="form-group">
													<label
														for="exampleInputEmail"
														class="form-control-label"
													>
														Teléfono
													</label>
													<input
														type="email"
														class="form-control"
														id="exampleInputEmail"
														placeholder="+595-991-365895"
													/>
												</div>
											</div>
											<div class="col-xs-12 col-lg-6">
												<div class="form-group">
													<label
														for="exampleInputEmail"
														class="form-control-label"
													>
														Edad
													</label>
													<input
														type="email"
														class="form-control"
														id="exampleInputEmail"
														placeholder="28"
													/>
												</div>
											</div>
											<div class="col-xs-12 col-lg-12">
												<div class="form-group">
													<label
														for="exampleTextarea"
														class="form-control-label"
													>
														Dirección
													</label>
													<textarea
														class="form-control"
														id="exampleTextarea"
														rows="4"
													></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h5>USUARIO</h5>
						</div>
						<div class="card-body">
							<div class="bt-wizard" id="besicwizard">
								<div class="tab-content">
									<div class="tab-pane show active" id="b-w-tab1">
										<form>
											<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Correo Eléctronico
												</label>
												<div class="col-sm-9">
													<input
														type="text"
														class="form-control"
														id="b-t-name"
														placeholder=""
													/>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Nombre de Usuario
												</label>
												<div class="col-sm-9">
													<input
														type="text"
														class="form-control"
														id="b-t-name"
														placeholder=""
													/>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Contraseña
												</label>
												<div class="col-sm-9">
													<input
														type="text"
														class="form-control"
														id="b-t-name"
														placeholder=""
													/>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Reescriba la Contraseña
												</label>
												<div class="col-sm-9">
													<input
														type="text"
														class="form-control"
														id="b-t-name"
														placeholder=""
													/>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-md-12">
					<div class="card">
						<div class="card-body">
							<button type="button" class="btn btn-primary btn-block">
								Guardar Cliente
							</button>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h5>Condición</h5>
						</div>
						<div class="card-body">
							<div class="bt-wizard" id="besicwizard">
								<div class="tab-content">
									<div class="tab-pane show active" id="b-w-tab1">
										<form>
											<div class="form-group row">
												<div class="col-sm-12">
													<select class="form-control">
														<option>Activo</option>
														<option>Suspendido</option>
														<option>Premium</option>
													</select>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Page>
	);
};

export default Create;
