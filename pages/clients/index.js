import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import Link from "next/link";

const Clients = ({ Children }) => {
	const [posts, setPosts] = useState([]);
	const [items, setItems] = useState({});
	const [open, setOpen] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	const handleClickAdd = () => {
		setOpen(!open);
	};

	const handleClick = () => {
		setOpen(!open);
	};

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	useEffect(() => {
		notiflix();
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/users/list", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setPosts(response.data);
			});
	}, []);

	const { SearchBar } = Search;

	function rankFormatter(cell, row, rowIndex, formatExtraData, dataField) {
		const onSubmitRemove = (id) => {
			const token = localStorage.usertoken;
			fetch("http://localhost:8000/api/giftcard/destroy", {
				method: "DELETE",
				body: JSON.stringify({
					id: id
				}),
				headers: {
					"Content-type": "application/json; charset=UTF-8",
					Accept: "application/json",
					Authorization: `Bearer ${token}`,
				},
			});
			Notiflix.Notify.Success("Giftcard Deleted");
			const interval = setInterval(() => {
				window.location.reload(false);
			}, 2000);
			return () => clearInterval(interval);
		};

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
			
				<Link href={{ pathname: '/orders/edit', query: { id: row.id }}} ><a><i class="icon feather icon-edit f-w-600 f-16 m-r-15 text-c-green"></i></a></Link>
				<i onClick={() => onSubmitRemove(row.id)} class="feather icon-trash-2 f-w-600 f-16 text-c-red"></i>
			</div>
		);
	}
	function imageurl(cell, row, rowIndex, formatExtraData, dataField) {

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				<img height="50" src={row.avatar} style={{borderRadius: 10}}/>
		
			</div>
		);
	}

	const onSubmitView = (id) => {
		event.preventDefault();
		const token = localStorage.usertoken;
		fetch("http://localhost:3333/api/v1/admin/categories/" + id, {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				return setItems(response);
			});

		handleClick();
	};

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Users List</title>
			</Head>
			<div class="row">
				<div class="col-xl-12 col-md-12">
					<div class="card table-card loading">
						<div class="card-header">
							<h5>Users</h5>
							<div class="card-header-right">
								<div class="btn-group card-option">
									<Link href="/clients/create">
									<a class="btn btn-md btn-info">
										Create User
									</a>
									</Link>
								</div>
							</div>
						</div>
						<div class="card-body p-0">
							<div class="table-responsive-sm table-responsive-xs table-responsive-md">
								<>
									<ToolkitProvider
										data={posts}
										keyField="name"
										columns={[
											{
												dataField: "id",
												text: "ID",
												sort: false,
											},
											{
												dataField: "avatar",
												text: "Imagen",
												sort: false,
												formatter: imageurl
											},
											{
												dataField: "name",
												text: "Username",
												sort: false,
											},
											
											{
												dataField: "name",
												text: "Name",
												sort: false,
											},
											{
												dataField: "email",
												text: "Email",
												sort: false,
											},
											{
												dataField: "bitcoin_wallet",
												text: "Bitcoin Wallet",
												sort: false,
											},
											{
												dataField: "paypal_email",
												text: "Paypal Email",
												sort: false,
											},
											{
												dataField: "created_at",
												text: "Created",
												sort: false,
											},
											{
												dataField: "status",
												text: "Status",
												sort: false,
											},
											{
												dataField: "edit",
												text: "Action",
												sort: false,
												formatter: rankFormatter,
												headerAttrs: { width: 50 },
												attrs: {
													width: 100,
													className: "EditRow",
												},
											},
										]}
										search
									>
										{(props) => (
											<div className="py-4">
												<div
													id="datatable-basic_filter"
													className="dataTables_filter px-4 pb-1"
												>
													<label>
														Buscar:
														<SearchBar
															className="form-control-sm"
															placeholder=""
															{...props.searchProps}
														/>
													</label>
												</div>
												<BootstrapTable
													{...props.baseProps}
													pagination={paginationFactory()}
													bordered={false}
												/>
											</div>
										)}
									</ToolkitProvider>
								</>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Page>
	);
};

export default Clients;
