import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import TextField from '@material-ui/core/TextField';
const Create = ({ children }) => {
	const [gifts, setGifts] = useState([]);
  const [users, setUsers] = useState({});
  const [title, setTitle] = useState({});
	const [open, setOpen] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	// const handleClickAdd = () => {
	// 	setOpen(!open);
	// };

	// const handleClick = () => {
	// 	setOpen(!open);
	// };

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/details", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
        setUsers(response.success);
			});
	
  }, []);



	const onSubmitgiftcard = (event) => {
		event.preventDefault();
		const token = localStorage.usertoken;
	
		fetch("http://localhost:8000/api/giftcard/redeem", {
			method: "POST",
			body: JSON.stringify({
				user_id: users.id,
				giftcode: formState.values.giftcode
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then((response) => response.json())
			.then(function (response) {
        setGifts(response[0])
			});
	};

	const onUpdategiftcard = (event) => {
		event.preventDefault();
		const token = localStorage.usertoken;
	
		fetch("http://localhost:8000/api/giftcard/update", {
			method: "PUT",
			body: JSON.stringify({
				id: gifts.id,
				end_date: formState.values.giftcode,
				status: 3
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then((response) => response.json())
			.then(function (response) {
      
			});
	};


	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Redeem GiftCard</title>
			</Head>
      <form onSubmit={onSubmitgiftcard}>
			<div class="row">
				<div class="col-xl-7 col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="bt-wizard" id="besicwizard">
								<div class="tab-content">
									<div class="tab-pane show active" id="b-w-tab1">
											<div class="form-group row">
												<label for="b-t-name" class="col-sm-12 col-form-label">
													Insert GiftCard Code
												</label>
												<div class="col-sm-12">
													<input
														type="text"
														class="form-control"
														name="giftcode"
														onChange={handleChange}
														value={formState.values.giftcode}
													/>
												</div>
											</div>
											{gifts.status & gifts.status === 1 ? <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													End Date
												</label>
												<div class="col-sm-9">
												<TextField
													id="date"
													label="End Date"
													type="date"
													name="end_date"
													onChange={handleChange}
													value={formState.values.end_date}
											
													InputLabelProps={{
														shrink: true,
													}}
												/>
												</div>
											</div> : null }
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="card">
						<div class="card-header">
							<h5>Details</h5>
						</div>
						<div class="card-body">
							<div class="bt-wizard" id="besicwizard">
								<div class="tab-content">
									<div class="tab-pane show active" id="b-w-tab1">
						
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													image
												</label>
												<div class="col-sm-9">
                        <img height="150" src={gifts.imageurl} />
												</div>
											</div>
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Title
												</label>
												<div class="col-sm-9">
                        {gifts.title}
												</div>
											</div>
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Description
												</label>
												<div class="col-sm-9">
                        {gifts.description}
												</div>
											</div>
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Price Usd
												</label>
												<div class="col-sm-9">
                        {gifts.prize_usd}
												</div>
											</div>
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													price Btc
												</label>
												<div class="col-sm-9">
                        {gifts.prize_btc}
												</div>
											</div>
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Currency USD
												</label>
												<div class="col-sm-9">
                        {gifts.currency_usd}
												</div>
											</div>
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Currency BTC
												</label>
												<div class="col-sm-9">
                        {gifts.currency_usd}
												</div>
											</div>
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Start Date
												</label>
												<div class="col-sm-9">
                        {gifts.start_date}
												</div>
											</div>
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													End Date
												</label>
												<div class="col-sm-9">
                        {gifts.end_date}
												</div>
											</div>
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Company
												</label>
												<div class="col-sm-9">
                        {gifts.company}
												</div>
											</div>
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Category
												</label>
												<div class="col-sm-9">
                        {gifts.category}
												</div>
											</div>
                      <div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Status
												</label>
												<div class="col-sm-9">
                        {gifts.status}
												</div>
											</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-md-12">
					<div class="card">
						<div class="card-body">
							{!gifts.status ? <button type="submit" class="btn btn-primary btn-block">
								Redeem GiftCard
							</button> : null}
							{gifts.status & gifts.status === 1 ? 
							<button onClick={onUpdategiftcard} type="button" class="btn btn-primary btn-block">
								Process GiftCard
							</button> : null}
							{gifts.status & gifts.status === 3 ? 
							<button type="button" class="btn btn-primary btn-block" disabled>
								Redeemeded GiftCard
							</button> : null}
						</div>
					</div>
				</div>
			</div>
      </form>
		</Page>
	);
};

export default Create;
