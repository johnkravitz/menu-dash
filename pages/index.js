import React, { useState, useEffect } from "react";
import Page from "../layout/auth";
import { login } from "../components/UserFunctions.js";
import { useRouter } from "next/router";

const SignIn = ({ children }) => {
	const router = useRouter();
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	// useEffect(() => {
	//   const token = localStorage.usertoken
	//   console.log(token,'token sign effect')
	//   if (token) {
	//     history.push(`/dashboard`);
	//   }
	// }, []);

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]:
					event.target.type === "checkbox"
						? event.target.checked
						: event.target.value,
			},
			touched: {
				...formState.touched,
				[event.target.name]: true,
			},
		}));
	};

	const handleSignIn = (event) => {
		event.preventDefault();
		const user = {
			email: formState.values.email,
			password: formState.values.password,
		};

		login(user).then((res) => {
			console.log(res, "datalogin");
			if (!res.token) {
				router.push("/");
			} else {
				router.push("/dashboard");
			}
		});
	};

	return (
		<Page>
			<div class="auth-content">
				<div class="card">
					<div class="row align-items-center text-center">
						<div class="col-md-12">
							<div class="card-body">
								<img
									src="assets/images/logo-dark.png"
									alt=""
									class="img-fluid mb-4"
								/>
								<h4 class="mb-3 f-w-400">Iniciar Sesión</h4>
								<form onSubmit={handleSignIn}>
									<div class="form-group mb-3">
										<input
											type="text"
											class="form-control"
											name="email"
											onChange={handleChange}
											value={formState.values.email}
											placeholder="Email"
										/>
									</div>
									<div class="form-group mb-4">
										<input
											type="password"
											class="form-control"
											name="password"
											onChange={handleChange}
											value={formState.values.password}
											placeholder="Contraseña"
										/>
									</div>
									<div class="custom-control custom-checkbox text-left mb-4 mt-2">
										<input
											type="checkbox"
											class="custom-control-input"
											id="customCheck1"
										/>
										<label class="custom-control-label" for="customCheck1">
											Save credentials.
										</label>
									</div>
									<button type="submit" class="btn btn-block btn-primary mb-4">
										Iniciar Sesión
									</button>
								</form>
								<p class="mb-2 text-muted">
									Olvido su contraseña?{" "}
									<a href="auth-reset-password.html" class="f-w-400">
										Reset
									</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Page>
	);
};
export default SignIn;
