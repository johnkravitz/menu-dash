import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../../layout/main";
import fetch from "node-fetch";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import CreateForm from "./create.js";
import EditForm from "./edit.js";

const Lines = ({ Children }) => {
	const [posts, setPosts] = useState([]);
	const [items, setItems] = useState({});
	const [open, setOpen] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	const handleClickAdd = () => {
		setOpen(!open);
	};

	const handleClick = () => {
		setOpen(!open);
	};

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	useEffect(() => {
		notiflix();
		const token = localStorage.usertoken;
		fetch("http://localhost:3333/api/v1/admin/lines", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setPosts(response);
			});
	}, []);

	const { SearchBar } = Search;

	function rankFormatter(cell, row, rowIndex, formatExtraData, dataField) {
		const onSubmitRemove = (id) => {
			const token = localStorage.usertoken;
			fetch("http://localhost:3333/api/v1/admin/lines/" + id, {
				method: "DELETE",
				headers: {
					"Content-type": "application/json; charset=UTF-8",
					Accept: "application/json",
					Authorization: `Bearer ${token}`,
				},
			});
			Notiflix.Notify.Success("Línea Eliminada");
			const interval = setInterval(() => {
				window.location.reload(false);
			}, 2000);
			return () => clearInterval(interval);
		};

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				<i onClick={() => onSubmitView(row.id)} className="far fa-edit"></i>
				<i
					onClick={() => onSubmitRemove(row.id)}
					className="fas fa-trash-alt ml-3"
				></i>
			</div>
		);
	}

	const onSubmitView = (id) => {
		event.preventDefault();
		const token = localStorage.usertoken;
		fetch("http://localhost:3333/api/v1/admin/lines/" + id, {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				return setItems(response);
			});

		handleClick();
	};

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Listado de Lineas</title>
			</Head>
			<div class="row">
				<div class="col-xl-8 col-md-12">
					<div class="card table-card loading">
						<div class="card-header">
							<h5>Líneas</h5>
						</div>
						<div class="card-body p-0">
							<div class="table-responsive-sm table-responsive-xs table-responsive-md">
								<>
									<ToolkitProvider
										data={posts}
										keyField="name"
										columns={[
											{
												dataField: "name",
												text: "Nombre",
												sort: false,
											},
											{
												dataField: "edit",
												text: "Acción",
												sort: false,
												formatter: rankFormatter,
												headerAttrs: { width: 50 },
												attrs: {
													width: 100,
													className: "EditRow",
												},
											},
										]}
										search
									>
										{(props) => (
											<div className="py-4">
												<div
													id="datatable-basic_filter"
													className="dataTables_filter px-4 pb-1"
												>
													<label>
														Buscar:
														<SearchBar
															className="form-control-sm"
															placeholder=""
															{...props.searchProps}
														/>
													</label>
												</div>
												<BootstrapTable
													{...props.baseProps}
													pagination={paginationFactory()}
													bordered={false}
												/>
											</div>
										)}
									</ToolkitProvider>
								</>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-12">
					<div class="card latest-update-card">
						<div class="card-header">
							{open && <h5>Crear Línea</h5>}
							{!open && <h5>Editar Línea</h5>}
							{!open && (
								<div class="card-header-right">
									<div class="btn-group card-option">
										<button
											type="button"
											onClick={handleClick}
											class="btn btn-md btn-info"
										>
											Crear Línea
										</button>
									</div>
								</div>
							)}
						</div>
						{open && <CreateForm />}
						{!open && <EditForm items={items} handleClickAdd />}
					</div>
				</div>
			</div>
		</Page>
	);
};

export default Lines;
