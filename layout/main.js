import Head from "next/head";
import React, { useState, useEffect } from "react";
import Sidebar from "../components/sidebar.js";
import Header from "../components/header.js";
import PropTypes from 'prop-types';
import { getUsers } from "../services/users.js";

const Main = (props) => {
	const [users, setUsers] = useState([]);
	const [childUser, setChildUser] = useState([]);
	const {children} = props;
	useEffect(() => {
		getUsers().then((response) => {
		  setUsers(response.success);
		});
	  }, []);
	return (
		<div>
			<Head>
				<title>Menufy</title>

				<meta charset="utf-8" />
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui"
				/>
				<meta http-equiv="X-UA-Compatible" content="IE=edge" />
				<meta name="description" content="" />
				<meta name="keywords" content="" />
				<meta name="author" content="Phoenixcoded" />

				<link
					rel="icon"
					href="/assets/images/favicon.png"
					type="image/x-icon"
				/>
				<link
					rel="stylesheet"
					href="/assets/css/plugins/react-bootstrap-table2-paginator.css"
				/>
				<link rel="stylesheet" href="/assets/css/style.css" />
				<link rel="stylesheet" href="/assets/css/plugins/animate.min.css" />

			</Head>

				<Sidebar users={users}/>
				<Header />
					<div className="pcoded-main-container">
						<div className="pcoded-content">
							<div className="page-header">
								<div className="page-block">
									<div className="row align-items-center">
										<div className="col-md-12">
											<div className="page-header-title">
												<h5 className="m-b-10">Dashboard</h5>
											</div>
											<ul className="breadcrumb">
												<li className="breadcrumb-item">
													<a href="index.html">
														<i className="feather icon-home"></i>
													</a>
												</li>
												<li className="breadcrumb-item">
													<a href="/">Home</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							
							{props.children}
							
						</div>
					</div>
				<script
					type="text/javascript"
					src="/assets/js/jquery-3.2.1.slim.min.js"
				></script>
				<script
					type="text/javascript"
					src="/assets/js/plugins/bootstrap.min.js"
				></script>
		</div>
	);
};
Main.propTypes = {
  children: PropTypes.node
};

export default Main;
