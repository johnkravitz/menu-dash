import React, { useState, useEffect } from "react";
import Link from "next/link";

const Sidebar = (props) => {
	const { users } = props;
	return (
		<nav className="pcoded-navbar menu-light">
			<div className="navbar-wrapper">
				<div className="navbar-content scroll-div">
					<div className="">
						<div className="main-menu-header">
						{users.avatar ? <img
								className="img-radius"
								src={users.avatar}
								alt="User-Profile-Image"
							/>: null }
							<div className="user-details">
								<div id="more-details">
									{users.name}<i className="fa fa-caret-down"></i>
								</div>
							</div>
						</div>
						<div className="collapse" id="nav-user-link">
							<ul className="list-unstyled">
								<li className="list-group-item">
									<a href="user-profile.html">
										<i className="feather icon-user m-r-5"></i>View Profile
									</a>
								</li>
								<li className="list-group-item">
									<a href="#!">
										<i className="feather icon-settings m-r-5"></i>Settings
									</a>
								</li>
								<li className="list-group-item">
									<a href="auth-normal-sign-in.html">
										<i className="feather icon-log-out m-r-5"></i>Logout
									</a>
								</li>
							</ul>
						</div>
					</div>

					<ul className="nav pcoded-inner-navbar">
						<li className="nav-item pcoded-menu-caption">
							<label>Dashboard</label>
						</li>
						<li className="nav-item">
							<Link href="/dashboard">
								<a className="nav-link">
									<span className="pcoded-micon">
										<i className="feather icon-home"></i>
									</span>
									<span className="pcoded-mtext">Inicio</span>
								</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link href="/pin" prefetch={false}>
								<a className="nav-link">
									<span className="pcoded-micon">
										<i className="feather icon-file-text"></i>
									</span>
									<span className="pcoded-mtext">Pin</span>
								</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link href="/menu">
								<a className="nav-link">
									<span className="pcoded-micon">
										<i className="feather icon-file-text"></i>
									</span>
									<span className="pcoded-mtext">Menú</span>
								</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link href="/orders/list">
								<a className="nav-link">
									<span className="pcoded-micon">
										<i className="feather icon-file-text"></i>
									</span>
									<span className="pcoded-mtext">Ordenes</span>
								</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link href="/collections">
								<a className="nav-link">
									<span className="pcoded-micon">
										<i className="feather icon-file-text"></i>
									</span>
									<span className="pcoded-mtext">Colección</span>
								</a>
							</Link>
						</li>
						<li className="nav-item">
							<Link href="/locals">
								<a className="nav-link">
									<span className="pcoded-micon">
										<i className="feather icon-file-text"></i>
									</span>
									<span className="pcoded-mtext">Locales</span>
								</a>
							</Link>
						</li>
						{/* <li className="nav-item">
							<Link href="/products">
								<a className="nav-link">
									<span className="pcoded-micon">
										<i className="feather icon-file-text"></i>
									</span>
									<span className="pcoded-mtext">Promociones</span>
								</a>
							</Link>
						</li> */}
						<li className="nav-item">
							<Link href="/payment">
								<a className="nav-link">
									<span className="pcoded-micon">
										<i className="feather icon-file-text"></i>
									</span>
									<span className="pcoded-mtext">Facturación</span>
								</a>
							</Link>
						</li>
						
						{users.status === '1' && (<li className="nav-item">
							<Link href="/clients">
								<a className="nav-link">
									<span className="pcoded-micon">
										<i className="feather icon-file-text"></i>
									</span>
									<span className="pcoded-mtext">Usuarios</span>
								</a>
							</Link>
						</li>
							)}
				
					</ul>
				</div>
			</div>
		</nav>
	);
};

export default Sidebar;
