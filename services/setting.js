export const BASE_URL =
  process.env.NODE_ENV === "production"
    ? "https://interuno.net"
    : "http://localhost:8000";
