import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Page from "../../layout/main";
import Link from "next/link";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import { useRouter } from "next/router";

const Dashboard = (props) => {
  const [tables, setTables] = useState([]);
  const [locals, setLocals] = useState([]);
  const [orders, setOrders] = useState([]);
  const [pins, setPins] = useState([]);
  const {users} = props;
  const router = useRouter();

  useEffect(() => {
    const token = localStorage.usertoken
console.log(users, 'users')
        fetch("http://localhost:8000/api/table/admintables", {
          method: "POST",
          headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
          },
        })
          .then(function (response) {
            return response.json();
          })
          .then(function (response) {
			  if(response){
				setTables(response);
			  }
          });
        fetch("http://localhost:8000/api/table/pin/adminlist", {
          method: "POST",
          body: JSON.stringify({
            admin_id: users
          }),
          headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
          },
        })
          .then(function (response) {
            return response.json();
          })
          .then(function (response) {
            setPins(response);
          });
        fetch("http://localhost:8000/api/local/list", {
          method: "POST",
          body: JSON.stringify({
            admin_id: users
          }),
          headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
          },
        })
          .then(function (response) {
            return response.json();
          })
          .then(function (response) {
            setLocals(response);
          });
        fetch("http://localhost:8000/api/order/adminlist", {
          method: "POST",
          body: JSON.stringify({
            admin_id: users
          }),
          headers: {
            "Content-type": "application/json; charset=UTF-8",
            Accept: "application/json",
            Authorization: `Bearer ${token}`,
          },
        })
          .then(function (response) {
            return response.json();
          })
          .then(function (response) {
            setOrders(response);
          });
        return () => {
          // clean up
        };
  }, []);

  const { SearchBar } = Search;

  function rankFormatter(cell, row, rowIndex, formatExtraData, dataField) {
    const onSubmitRemove = (id) => {
      const token = localStorage.usertoken;
      fetch("http://localhost:8000/api/giftcard/destroy", {
        method: "DELETE",
        body: JSON.stringify({
          id: id,
        }),
        headers: {
          "Content-type": "application/json; charset=UTF-8",
          Accept: "application/json",
          Authorization: `Bearer ${token}`,
        },
      });
      Notiflix.Notify.Success("Giftcard Deleted");
      const interval = setInterval(() => {
        window.location.reload(false);
      }, 2000);
      return () => clearInterval(interval);
    };

    return (
      <div
        style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
      >
        <Link href={{ pathname: "/orders", query: { id: row.tbl_pin } }}>
          <a>
            <i className="icon feather icon-eye f-w-600 f-16 m-r-15 text-c-green"></i>
          </a>
        </Link>
        <i
          onClick={() => onSubmitRemove(row.id)}
          className="feather icon-trash-2 f-w-600 f-16 text-c-red"
        ></i>
      </div>
    );
  }
  function imageurl(cell, row, rowIndex, formatExtraData, dataField) {
    return (
      <img
        src="assets/images/favicon.png"
        style={{
          height: 38,
          borderRadius: 10,
          border: "1px solid #ced4da",
          padding: 6,
        }}
      />
    );
  }
  const notiflix = () => {
    Notiflix.Block.Standard(".loading", "Processing...");
    Notiflix.Block.Remove(".loading", 1000);
  };
  return (
    <Page>
      <div className="row">
        <div className="col-lg-12 col-md-12">
          <div className="row">
            <div className="col-xs-6 col-sm-6 col-md-3">
              <div className="card">
                <div className="card-body">
                  <div className="row align-items-center">
                    <div className="col-8">
                      <h4 className="text-c-purple">{tables.length}</h4>
                      <h6 className="text-muted m-b-0">Usuarios Online</h6>
                    </div>
                    <div className="col-4 text-right">
                      <i className="feather icon-bar-chart-2 f-28"></i>
                    </div>
                  </div>
                </div>
                <div className="card-footer bg-dark">
                  <div className="row align-items-center">
                    <div className="col-9">
                      <p className="text-white m-b-0">Total</p>
                    </div>
                    <div className="col-3 text-right">
                      <i className="feather icon-trending-up text-white f-16"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xs-6 col-sm-6 col-md-3">
              <div className="card">
                <div className="card-body">
                  <div className="row align-items-center">
                    <div className="col-8">
                      <h4 className="text-c-green">{pins.length}</h4>
                      <h6 className="text-muted m-b-0">Pins Disponibles</h6>
                    </div>
                    <div className="col-4 text-right">
                      <i className="feather icon-file-text f-28"></i>
                    </div>
                  </div>
                </div>
                <div className="card-footer bg-dark">
                  <div className="row align-items-center">
                    <div className="col-9">
                      <p className="text-white m-b-0">Total</p>
                    </div>
                    <div className="col-3 text-right">
                      <i className="feather icon-trending-up text-white f-16"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xs-6 col-sm-6 col-md-3">
              <div className="card">
                <div className="card-body">
                  <div className="row align-items-center">
                    <div className="col-8">
                      <h4 className="text-c-red">{locals.length}</h4>
                      <h6 className="text-muted m-b-0">Compañia</h6>
                    </div>
                    <div className="col-4 text-right">
                      <i className="feather icon-calendar f-28"></i>
                    </div>
                  </div>
                </div>
                <div className="card-footer bg-dark">
                  <div className="row align-items-center">
                    <div className="col-9">
                      <p className="text-white m-b-0">Total</p>
                    </div>
                    <div className="col-3 text-right">
                      <i className="feather icon-trending-down text-white f-16"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-xs-6 col-sm-6 col-md-3">
              <div className="card">
                <div className="card-body">
                  <div className="row align-items-center">
                    <div className="col-8">
                      <h4 className="text-c-blue">{orders.length}</h4>
                      <h6 className="text-muted m-b-0">Total Ordenes</h6>
                    </div>
                    <div className="col-4 text-right">
                      <i className="feather icon-thumbs-down f-28"></i>
                    </div>
                  </div>
                </div>
                <div className="card-footer bg-dark">
                  <div className="row align-items-center">
                    <div className="col-9">
                      <p className="text-white m-b-0">Stock</p>
                    </div>
                    <div className="col-3 text-right">
                      <i className="feather icon-trending-down text-white f-16"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xl-9 col-md-12">
          <div className="card table-card">
            <div className="card-header">
              <h5>Lobby</h5>
              <div className="card-header-right">
                <div className="btn-group card-option">
                  <Link href="/pin/create">
                    <a className="btn btn-md btn-light mr-2">Pin</a>
                  </Link>
                  <Link href="/menu/create">
                    <a className="btn btn-md btn-light">Nuevo Item</a>
                  </Link>
                  <Link href="/menu">
                    <a className="btn btn-md btn-dark ml-2">Menú</a>
                  </Link>
                </div>
              </div>
            </div>
            <div className="card-body p-0">
              <div className="table-responsive-sm table-responsive-xs table-responsive-md">
                <>
                  <ToolkitProvider
                    data={tables}
                    keyField="tbl_number"
                    columns={[
                      {
                        dataField: "imageurl",
                        text: "IMAGE",
                        sort: false,
                        formatter: imageurl,
                      },
                      {
                        dataField: "tbl_number",
                        text: "Nº DE MESA",
                        sort: false,
                      },
                      {
                        dataField: "tbl_pin",
                        text: "PIN",
                        sort: false,
                      },
                      {
                        dataField: "client",
                        text: "CLIENTE",
                        sort: false,
                      },
                      {
                        dataField: "number_clients",
                        text: "Nº DE CLIENTES",
                        sort: false,
                      },
                      {
                        dataField: "payment",
                        text: "METODO DE PAGO",
                        sort: false,
                      },
                      {
                        dataField: "type",
                        text: "TIPO DE MESA",
                        sort: false,
                      },
                      {
                        dataField: "status",
                        text: "STATUS",
                        sort: false,
                      },
                      {
                        dataField: "edit",
                        text: "VER ORDENES",
                        sort: false,
                        formatter: rankFormatter,
                        headerAttrs: { width: 50 },
                        attrs: {
                          width: 100,
                          className: "EditRow",
                        },
                      },
                    ]}
                    search
                  >
                    {(props) => (
                      <div className="py-4">
                        <div
                          id="datatable-basic_filter"
                          className="dataTables_filter px-4 pb-1"
                        >
                          <label>
                            Buscar:
                            <SearchBar
                              className="form-control-sm"
                              placeholder=""
                              {...props.searchProps}
                            />
                          </label>
                        </div>
                        <BootstrapTable
                          {...props.baseProps}
                          pagination={paginationFactory()}
                          bordered={false}
                        />
                      </div>
                    )}
                  </ToolkitProvider>
                </>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xl-3 col-md-12 no-gutters">
          <div className="col-xl-12 col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="row align-items-center">
                  <div className="col-6">
                    <h3>16,756</h3>
                    <h6 className="text-muted m-b-0">
                      Usuarios Hoy
                      <i className="fa fa-caret-down text-c-red m-l-10"></i>
                    </h6>
                  </div>
                  <div className="col-6">
                    <div
                      id="seo-chart1"
                      className="d-flex align-items-end"
                    ></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-12 col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="row align-items-center">
                  <div className="col-6">
                    <h3>49.54%</h3>
                    <h6 className="text-muted m-b-0">
                      Comisiones
                      <i className="fa fa-caret-up text-c-green m-l-10"></i>
                    </h6>
                  </div>
                  <div className="col-6">
                    <div
                      id="seo-chart2"
                      className="d-flex align-items-end"
                    ></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-12 col-md-12">
            <div className="card">
              <div className="card-body">
                <div className="row align-items-center">
                  <div className="col-6">
                    <h3>15564</h3>
                    <h6 className="text-muted m-b-0">
                      Platos
                      <i className="fa fa-caret-down text-c-red m-l-10"></i>
                    </h6>
                  </div>
                  <div className="col-6">
                    <div
                      id="seo-chart3"
                      className="d-flex align-items-end"
                    ></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Page>
  );
};
export default Dashboard;
