import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import TextField from '@material-ui/core/TextField';

const Edit = ({ url: { query: { id } } }) => {
  const [users, setUsers] = useState([]);
	const [items, setItems] = useState({});
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	// const handleClickAdd = () => {
	// 	setOpen(!open);
	// };

	// const handleClick = () => {
	// 	setOpen(!open);
	// };

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	

  useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/local/listby", {
			method: "POST",
			body: JSON.stringify({
				id: id
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
					setItems(response[0])
			});
	
	}, []);


	useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/details", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
        setUsers(response.success);
			});
	
	}, []);
  
	const onSubmitEditLocal = (event) => {
		event.preventDefault();
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/local/update", {
			method: "PUT",
			body: JSON.stringify({
				admin_id: users.id,
				local_id: ranCode,
				name: formState.values.name,
				status: formState.values.status
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then((response) => response.json())
			.then(function (response) {
				// if (response[0]) {
				// 	Notiflix.Notify.Failure(response[0].message);
				// } else {
				// 	Notiflix.Notify.Success("Marca Creada Exitosamente");
				// 	const interval = setInterval(() => {
				// 		window.location.reload(false);
				// 	}, 2000);
				// 	return () => clearInterval(interval);
				// }
			});
	};


	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Editar Local</title>
			</Head>
			<form onSubmit={onSubmitEditLocal}>
			<div class="row">
				<div class="col-xl-4 col-md-12"></div>
				<div class="col-xl-4 col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="bt-wizard" id="besicwizard">
								<div class="tab-content">
									<div class="tab-pane show active" id="b-w-tab1">
									
											
											{users.status === '1' && (
												<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-12 col-form-label">
													Cliente
												</label>
												<div class="col-sm-12">
													<select class="form-control"
														name="user_id"
														onChange={handleChange}>
														<option value={formState.values.user_id = "1"}>Activo</option>
														<option value={formState.values.user_id = "2"}>Inactivo</option>
													</select>
												</div>
											</div>
											)}
											<div class="form-group row">
												<label for="b-t-name" class="col-sm-12 col-form-label">
													Locales
												</label>
												<div class="col-sm-12">
													<input
														type="text"
														class="form-control"
														name="name"
														onChange={handleChange}
														value={formState.values.type ? items.name : items.name}
													/>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Status
												</label>
												<div class="col-sm-9">
													<select class="form-control"
														name="status"
														onChange={handleChange}>
														<option value={formState.values.status = "1"}>Activo</option>
														<option value={formState.values.status = "2"}>Inactivo</option>
													</select>
												</div>
											</div>
											<button type="submit" class="btn btn-primary btn-block">
												Guardar Local
											</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-12"></div>
		
			</div>
			</form>
		</Page>
	);
};

export default Edit;
