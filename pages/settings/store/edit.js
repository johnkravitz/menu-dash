import React, { useState } from "react";
import Notiflix from "notiflix";

const Edit = (props) => {
	const { items } = props;
	const [open, setOpen] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	const onSubmitEdit = (id) => {
		event.preventDefault();
		const token = localStorage.usertoken;
		fetch("http://localhost:3333/api/v1/admin/brands/" + id, {
			method: "PATCH",
			body: JSON.stringify({
				name: formState.values.name,
				code: formState.values.code,
				description: formState.values.description,
				order: formState.values.order,
				show: formState.values.show,
				page: formState.values.page,
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				if (response[0]) {
					Notiflix.Notify.Failure(response[0].message);
				} else {
					Notiflix.Notify.Success("Categoria Actualizada");
					const interval = setInterval(() => {
						window.location.reload(false);
					}, 2000);
					return () => clearInterval(interval);
				}
			});
	};
	const handleClickAdd = () => {
		setOpen(!open);
	};

	const handleClick = () => {
		setOpen(open);
	};

	return (
		<div class="card-body">
			<form
				id="upload-widget"
				method="post"
				max-lenght="2"
				action="/create"
				class="dropzone"
			></form>
			<div class="text-center">
				<button
					type="button"
					class="btn btn-primary waves-effect waves-light p-l-50 p-r-50 m-t-30"
				>
					Subir
				</button>
			</div>
		</div>
	);
};

export default Edit;
