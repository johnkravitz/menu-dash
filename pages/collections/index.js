import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import Link from "next/link";
import { useRouter } from "next/router";

const Collections = ({ Children }) => {
	const [collections, setCollections] = useState([]);
	const [users, setUsers] = useState([]);
	const router = useRouter();
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/details", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setUsers(response.success);
				fetch("http://localhost:8000/api/collection/adminlist", {
					method: "POST",
					body: JSON.stringify({
						admin_id: response.success.id
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
						Accept: "application/json",
						Authorization: `Bearer ${token}`,
					},
				})
					.then(function (response) {
						return response.json();
					})
					.then(function (response) {
						setCollections(response);
					});
			});
	
	}, []);

	const { SearchBar } = Search;
const editgiftCard = (id) => {
	router.push({
		pathname: '/orders/edit',
		query: { id: id}
});
}
	function rankFormatter(cell, row, rowIndex, formatExtraData, dataField) {
		const onSubmitRemove = (id) => {
			const token = localStorage.usertoken;
			fetch("http://localhost:8000/api/collection/destroy", {
				method: "DELETE",
				body: JSON.stringify({
					id: id
				}),
				headers: {
					"Content-type": "application/json; charset=UTF-8",
					Accept: "application/json",
					Authorization: `Bearer ${token}`,
				},
			});
			Notiflix.Notify.Success("Collection Deleted");
			const interval = setInterval(() => {
				window.location.reload(false);
			}, 2000);
			return () => clearInterval(interval);
		};

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
			
				<Link href={{ pathname: '/collections/edit', query: { id: row.id }}} ><a><i className="icon feather icon-edit f-w-600 f-16 m-r-15 text-c-green"></i></a></Link>
				<i onClick={() => onSubmitRemove(row.id)} className="feather icon-trash-2 f-w-600 f-16 text-c-red"></i>
			</div>
		);
	}
	function imageurl(cell, row, rowIndex, formatExtraData, dataField) {

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				<img height="50" src={row.imageurl} style={{borderRadius: 10}}/>
		
			</div>
		);
	}

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Listado de Colecciones</title>
			</Head>
			<div className="row">
				<div className="col-xl-12 col-md-12">
					<div className="card table-card loading">
						<div className="card-header">
							<h5>Categorias de Productos</h5>
							<div className="card-header-right">
								<div className="btn-group card-option">
									<Link href="/collections/create">
									<a  className="btn btn-md btn-dark">
										Crear Colección
									</a>
									</Link>
								</div>
							</div>
						</div>
						<div className="card-body p-0">
							<div className="table-responsive-sm table-responsive-xs table-responsive-md">
								<>
									<ToolkitProvider
										data={collections}
										keyField="name"
										columns={[
											{
												dataField: "local_id",
												text: "IMAGEN",
												sort: false,
												formatter: imageurl,
											},
											{
												dataField: "local_id",
												text: "LOCAL ID",
												sort: false
											},
											{
												dataField: "type",
												text: "NOMBRE",
												sort: false,
											},
											{
												dataField: "status",
												text: "STATUS",
												sort: false,
											},
											{
												dataField: "edit",
												text: "Acción",
												sort: false,
												formatter: rankFormatter,
												headerAttrs: { width: 50 },
												attrs: {
													width: 100,
													className: "EditRow",
												},
											},
										]}
										search
									>
										{(props) => (
											<div className="py-4">
												<div
													id="datatable-basic_filter"
													className="dataTables_filter px-4 pb-1"
												>
													<label>
														Buscar:
														<SearchBar
															className="form-control-sm"
															placeholder=""
															{...props.searchProps}
														/>
													</label>
												</div>
												<BootstrapTable
													{...props.baseProps}
													pagination={paginationFactory()}
													bordered={false}
												/>
											</div>
										)}
									</ToolkitProvider>
								</>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Page>
	);
};

export default Collections;
