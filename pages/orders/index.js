import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import Link from "next/link";

const Orders = ({
	url: {
		query: { id },
	},
}) => {
	const [items, setItems] = useState([]);
	const [orders, setOrders] = useState([]);
	const [users, setUsers] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]:
					event.target.type === "checkbox"
						? event.target.checked
						: event.target.value,
				[event.target.name]:
					event.target.type === "file" ? event.target.file : event.target.value,
			},
		}));
	};
	useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/details", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setUsers(response.success)
			});
	
	}, []);

	useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/order/list", {
			method: "POST",
			body: JSON.stringify({
				tbl_pin: id,
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setOrders(response[0]);
			});
	}, []);

	const onSubmitItem = (event, total_amount, local_id) => {
		event.preventDefault();
		const token = localStorage.usertoken;

		fetch("http://localhost:8000/api/order/update", {
			method: "PATCH",
			body: JSON.stringify({
				user_id: users.id,
				local_id: local_id,
				total_amount: total_amount,
				status: formState.values.status,
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setOrders(response[0]);
			});
	};

	useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/order/adminitems", {
			method: "POST",
			body: JSON.stringify({
				tbl_pin: id,
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setItems(response);
			});
	}, []);

	const { SearchBar } = Search;

	function rankFormatter(cell, row, rowIndex, formatExtraData, dataField) {
		const onSubmitRemove = (iddelete) => {
			const token = localStorage.usertoken;
			fetch("http://localhost:8000/api/order/deleteitem", {
				method: "post",
				body: JSON.stringify({
					id: iddelete,
				}),
				headers: {
					"Content-type": "application/json; charset=UTF-8",
					Accept: "application/json",
					Authorization: `Bearer ${token}`,
				},
			});
			Notiflix.Notify.Success("Orden Eliminada");
			// const interval = setInterval(() => {
			// 	window.location.reload(false);
			// }, 2000);
			// return () => clearInterval(interval);
		};

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				{/* <i onClick={() => onSubmitView(row.id)} className="far fa-edit"></i> */}
				<i
					onClick={() => onSubmitRemove(row.id)}
					className="feather icon-trash-2 f-w-600 f-16 text-c-red"
				></i>
			</div>
		);
	}
	function imageurl(cell, row, rowIndex, formatExtraData, dataField) {
		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				<img height="50" src={row.imageurl} style={{ borderRadius: 10 }} />
			</div>
		);
	}

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Listado de Ordenes</title>
			</Head>
			<div className="row">
				<div className="col-xl-4 col-md-4">
					<div className="card">
						<div className="card-header">
							<h5>Total a pagar</h5>
						</div>
						<div className="card-body">
							<div className="row">
								<div className="col-12">
									<h3>{orders.total_amount}</h3>
								</div>
								<form onSubmit={()=> onSubmitItem(orders.total_amount, orders.local_id)}>
									<div class="form-group row">
											<label for="b-t-pwd" class="col-sm-12 col-form-label">
												Procesar Orden
											</label>
											<div class="col-sm-6">
												<select
													class="form-control"
													name="status"
													onChange={handleChange}
												>
													<option value={(formState.values.status = "2")}>
														Procesar
													</option>
												</select>
											</div>
											
											<div class="col-sm-6">
												<div className="btn-group card-option">
													<button className="btn btn-md btn-dark" type="submit">Confirmar</button>
												</div>
											</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div className="card">
						<div className="card-header">
							<h5>Details</h5>
						</div>
						<div className="card-body">
							<div className="row">
								<div className="col-6">Usuario</div>
								<div className="col-6">{orders.user_id}</div>
								<div className="col-6">Status</div>
								<div className="col-6">{orders.status == '1' ? 'Sin Procesar' : 'Sin Procesar'}</div>
								<div className="col-6">Mesa</div>
								<div className="col-6">{orders.tbl_number}</div>
								<div className="col-6">Pin</div>
								<div className="col-6">{orders.tbl_pin}</div>
							</div>
						</div>
					</div>
					<div className="card">
						<div className="card-header">
							<h5>Comentario</h5>
						</div>
						<div className="card-body">
							<div className="row">
								<div className="col-12">{orders.comment}</div>
							</div>
						</div>
					</div>
				</div>
				<div className="col-xl-8 col-md-8">
					<div className="card table-card loading">
						<div className="card-header">
							<h5>Items</h5>
							<div className="card-header-right">
								<div className="btn-group card-option">
									<Link href="/orders/delete">
										<a className="btn btn-md btn-dark">Eliminar Orden</a>
									</Link>
								</div>
							</div>
						</div>
						<div className="card-body p-0">
							<div className="table-responsive-sm table-responsive-xs table-responsive-md">
								<>
									<ToolkitProvider
										data={items}
										keyField="name"
										columns={[
											{
												dataField: "imageurl",
												text: "Imagen",
												sort: false,
												formatter: imageurl,
											},
											{
												dataField: "name",
												text: "Nombre",
												sort: false,
											},
											{
												dataField: "quantity",
												text: "Cantidad",
												sort: false,
											},
											{
												dataField: "price",
												text: "Precio",
												sort: false,
											},
											{
												dataField: "edit",
												text: "Acción",
												sort: false,
												formatter: rankFormatter,
												headerAttrs: { width: 50 },
												attrs: {
													width: 100,
													className: "EditRow",
												},
											},
										]}
										search
									>
										{(props) => (
											<div className="py-4">
												<div
													id="datatable-basic_filter"
													className="dataTables_filter px-4 pb-1"
												>
													<label>
														Buscar:
														<SearchBar
															className="form-control-sm"
															placeholder=""
															{...props.searchProps}
														/>
													</label>
												</div>
												<BootstrapTable
													{...props.baseProps}
													pagination={paginationFactory()}
													bordered={false}
												/>
											</div>
										)}
									</ToolkitProvider>
								</>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Page>
	);
};

export default Orders;
