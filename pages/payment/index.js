import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import Link from "next/link";

const Payment = ({ url: { query: { id } } })  => {
	const [items, setItems] = useState([]);
	const [orders, setOrders] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});




	const { SearchBar } = Search;

	function rankFormatter(cell, row, rowIndex, formatExtraData, dataField) {
		const onSubmitRemove = (iddelete) => {
			const token = localStorage.usertoken;
			fetch("http://localhost:8000/api/order/deleteitem", {
				method: "post",
				body: JSON.stringify({
					id: iddelete
				}),
				headers: {
					"Content-type": "application/json; charset=UTF-8",
					Accept: "application/json",
					Authorization: `Bearer ${token}`,
				},
			});
			Notiflix.Notify.Success("Orden Eliminada");
			// const interval = setInterval(() => {
			// 	window.location.reload(false);
			// }, 2000);
			// return () => clearInterval(interval);
		};

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				{/* <i onClick={() => onSubmitView(row.id)} className="far fa-edit"></i> */}
				<i
					onClick={() => onSubmitRemove(row.id)}
					className="feather icon-trash-2 f-w-600 f-16 text-c-red"
				></i>
			</div>
		);
	}
	function imageurl(cell, row, rowIndex, formatExtraData, dataField) {

		return (
			<div
				style={{ textAlign: "center", cursor: "pointer", lineHeight: "normal" }}
			>
				<img height="50" src={row.imageurl} style={{borderRadius: 10}}/>
		
			</div>
		);
	}

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Listado de Ordenes</title>
			</Head>
			<div class="row">
			<div class="col-xl-4 col-md-4">
			<div class="card">
					<div class="card-header">
					<h5>Total a pagar</h5>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-12">
								<h3>{orders.total_amount}</h3>
							</div>
						</div>
					</div>
				</div>
				<div class="card">
					<div class="card-header">
					<h5>Details</h5>
					</div>
					<div class="card-body">
						<div class="row">
							<div class="col-6">Usuario</div>
						</div>
					</div>
				</div>
			</div>
				<div class="col-xl-8 col-md-8">
					<div class="card table-card loading">
						<div class="card-header">
							<h5>Pagos Realizados</h5>
						</div>
						<div class="card-body p-0">
							<div class="table-responsive-sm table-responsive-xs table-responsive-md">
								<>
									<ToolkitProvider
										data={items}
										keyField="name"
										columns={[
											{
												dataField: "imageurl",
												text: "Codigo",
												sort: false,
											},
											{
												dataField: "name",
												text: "Descripción",
												sort: false,
											},
											{
												dataField: "quantity",
												text: "Invoice",
												sort: false,
											},
											{
												dataField: "price",
												text: "Precio",
												sort: false,
											},
											{
												dataField: "edit",
												text: "Acción",
												sort: false,
												formatter: rankFormatter,
												headerAttrs: { width: 50 },
												attrs: {
													width: 100,
													className: "EditRow",
												},
											},
										]}
										search
									>
										{(props) => (
											<div className="py-4">
												<div
													id="datatable-basic_filter"
													className="dataTables_filter px-4 pb-1"
												>
													<label>
														Buscar:
														<SearchBar
															className="form-control-sm"
															placeholder=""
															{...props.searchProps}
														/>
													</label>
												</div>
												<BootstrapTable
													{...props.baseProps}
													pagination={paginationFactory()}
													bordered={false}
												/>
											</div>
										)}
									</ToolkitProvider>
								</>
							</div>
						</div>
					</div>
				</div>
			</div>
		</Page>
	);
};

export default Payment;
