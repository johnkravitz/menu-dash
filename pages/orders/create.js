import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import EditForm from "./edit.js";
import { DropzoneArea } from "material-ui-dropzone";

const Create = ({ children }) => {
	const [users, setUsers] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	// const handleClickAdd = () => {
	// 	setOpen(!open);
	// };

	// const handleClick = () => {
	// 	setOpen(!open);
	// };

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]:
					event.target.type === "checkbox"
						? event.target.checked
						: event.target.value,
				[event.target.name]:
					event.target.type === "file" ? event.target.file : event.target.value,
			},
		}));
	};

	useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/details", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
        setUsers(response.success);
			});
	
	}, []);


	const onSubmitOrder = (event) => {
		event.preventDefault();
		const token = localStorage.usertoken;

		fetch("http://localhost:8000/api/order/create", {
			method: "POST",
			body: JSON.stringify({
				user_id: users.id,
				local_id: 1,
				tbl_pin: formState.values.tbl_pin,
				tbl_number: formState.values.tbl_number,
				name: formState.values.name,
				order_items: order_items,
				total_amount: formState.values.total_amount,
				status: formState.values.status,
				comment: formState.values.comment
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then((response) => response.json())
			.then(function (response) {
				// if (response[0]) {
				// 	Notiflix.Notify.Failure(response[0].message);
				// } else {
				// 	Notiflix.Notify.Success("Producto creado Exitosamente");
				// 	const interval = setInterval(() => {
				// 		window.location.reload(false);
				// 	}, 2000);
				// 	return () => clearInterval(interval);
				// }
			});
	};

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Crear Item</title>
			</Head>
			<form onSubmit={onSubmitOrder}>
				<div class="row">
					<div class="col-xl-7 col-md-12">
						<div class="card">
							<div class="card-body">
								<div class="bt-wizard" id="besicwizard">
									<div class="tab-content">
										<div class="tab-pane show active" id="b-w-tab1">
											<div class="form-group row">
												<label for="b-t-name" class="col-sm-12 col-form-label">
													Imagen
												</label>
												<div class="col-sm-12">
													<input
														type="text"
														class="form-control"
														name="imageurl"
														onChange={handleChange}
														value={formState.values.imageurl}
													/>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-name" class="col-sm-12 col-form-label">
													Nombre
												</label>
												<div class="col-sm-12">
													<input
														type="text"
														class="form-control"
														name="name"
														onChange={handleChange}
														value={formState.values.name}
													/>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-name" class="col-sm-12 col-form-label">
													Descripción
												</label>
												<div class="col-sm-12">
												<textarea
														rows="3"
														class="form-control"
														name="description"
														onChange={handleChange}
														value={formState.values.description}
													></textarea>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-name" class="col-sm-12 col-form-label">
													Ingredientes
												</label>
												<div class="col-sm-12">
												<textarea
														rows="3"
														class="form-control"
														name="ingredients"
														onChange={handleChange}
														value={formState.values.ingredients}
													></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h5>Precio</h5>
							</div>
							<div class="card-body">
								<div class="bt-wizard" id="besicwizard">
									<div class="tab-content">
										<div class="tab-pane show active" id="b-w-tab1">
											<form>
												<div class="form-group row">
													<label for="b-t-pwd" class="col-sm-3 col-form-label">
														Precio
													</label>
													<div class="col-sm-9">
														<input
															type="text"
															class="form-control"
															name="price"
															onChange={handleChange}
															value={formState.values.price}
														/>
													</div>
												</div>
												<div class="form-group row">
													<div class="col-3 col-sm-6 col-md-2">
														<div class="switch d-inline m-r-10">
															<input
																type="checkbox"
																id="checked-default"
																class="form-control"
																name="getpromo"
																onChange={handleChange}
																value={
																	formState.values.getpromo
																		? formState.values.getpromo
																		: false
																}
															/>
															<label
																class="cr form-label"
																for="checked-default"
															></label>
														</div>
														<label for="b-t-pwd" class="form-label">
															Oferta
														</label>
													</div>
												</div>
											</form>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="card">
							<div class="card-header">
								<h5>Detalles</h5>
							</div>
							<div class="card-body">
								<div class="bt-wizard" id="besicwizard">
									<div class="tab-content">
										<div class="tab-pane show active" id="b-w-tab1">
											<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Status
												</label>
												<div class="col-sm-9">
													<select class="form-control"
														name="status"
														onChange={handleChange}>
														<option value={formState.values.status = "1"}>Activo</option>
														<option value={formState.values.status = "2"}>Inactivo</option>
													</select>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Tipo
												</label>
												<div class="col-sm-9">
													<select class="form-control"
														name="type"
														onChange={handleChange}>
														<option value={formState.values.type = "drink"}>Comida</option>
														<option value={formState.values.type = "food"}>Bebida</option>
													</select>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Moneda
												</label>
												<div class="col-sm-9">
													<select class="form-control"
														name="currency"
														onChange={handleChange}>
														<option value={formState.values.currency = "usd"}>USD</option>
														<option value={formState.values.currency = "bs"}>BOLIVARES</option>
													</select>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-name" class="col-sm-12 col-form-label">
													Comentario
												</label>
												<div class="col-sm-12">
													<textarea
														rows="3"
														class="form-control"
														name="comment"
														onChange={handleChange}
														value={formState.values.comment}
													></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-xl-3 col-md-12">
						<div class="card">
							<div class="card-body">
								<button type="submit" class="btn btn-primary btn-block">
									Guardar Item
								</button>
							</div>
						</div></div>
				</div>
			</form>
		</Page>
	);
};

export default Create;
