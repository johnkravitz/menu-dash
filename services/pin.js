import { BASE_URL } from "./setting";

const ENDPOINT = `${BASE_URL}/api/table/pin`;
const ENDPOINT_LIST = `${BASE_URL}/api/table/pin/adminlist`;
const ENDPOINT_UPDATE = `${BASE_URL}/api/table/pin/update`;
const ENDPOINT_DESTROY = `${BASE_URL}/api/pin/destroy`;

export async function getPins(id) {
  const token = localStorage.usertoken;

  const response = await fetch(ENDPOINT_LIST, {
    method: "POST",
    body: JSON.stringify({
      admin_id: id,
    }),
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
  });

  const pins = await response.json();
  return pins;
}

export async function addPin(ranCode, id, local_id, tbl_number, status) {
  const token = localStorage.usertoken;
  const res = await fetch(`${ENDPOINT}`, {
    method: "POST",
    body: JSON.stringify({
      admin_id: id,
      local_id: local_id,
      tbl_pin: ranCode,
      tbl_number: tbl_number,
      status: status
    }),
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    }
  });
  const pins = await res.json();
  return pins;
}

export async function editPin(id, data) {
  const token = localStorage.usertoken;
  const alphabet=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

  let ranletter1 = alphabet[Math.floor(Math.random() * alphabet.length)];
  let ranletter2 = alphabet[Math.floor(Math.random() * alphabet.length)];
  let ranNum = Math.floor((Math.random() * 99999999999) * 7);
  let ranCode = ranletter1 + ranletter2 + ranNum;

  const res = await fetch(`${ENDPOINT_UPDATE}/` + id, {
    method: "PATCH",
    body: JSON.stringify({
      admin_id: users.id,
      local_id: formState.values.local_id,
      tbl_pin: ranCode,
      tbl_number: formState.values.tbl_number,
      status: formState.values.status
    }),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    }
  });
  const pin = await res.json();
  return pin;
}

export async function removePin(id) {
  const token = localStorage.usertoken;

  const response = await fetch(`${ENDPOINT_DESTROY}`, {
    method: "DELETE",
    body: JSON.stringify({
      id: id
    }),
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
  });

  const pin = await response.json();
  return pin;
}
