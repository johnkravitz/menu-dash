import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";
import TextField from '@material-ui/core/TextField';

const Edit = ({ url: { query: { id } } }) => {
  const [users, setUsers] = useState([]);
	const [items, setItems] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/collection/listby", {
			method: "POST",
			body: JSON.stringify({
				id: id
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
					setItems(response[0])
			});
	
	}, []);


	useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/details", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
        setUsers(response.success);
			});
	
	}, []);

  
	const onSubmitLocal = (event) => {
		event.preventDefault();
		const token = localStorage.usertoken;
	
		fetch("http://localhost:8000/api/collection/update", {
			method: "PUT",
			body: JSON.stringify({
				admin_id: users.id,
				local_id: 1,
				type: formState.values.type,
				status: formState.values.status,
				imageurl: formState.values.imageurl
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then((response) => response.json())
			.then(function (response) {
				// if (response[0]) {
				// 	Notiflix.Notify.Failure(response[0].message);
				// } else {
				// 	Notiflix.Notify.Success("Marca Creada Exitosamente");
				// 	const interval = setInterval(() => {
				// 		window.location.reload(false);
				// 	}, 2000);
				// 	return () => clearInterval(interval);
				// }
			});
	};


	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Crear Colección</title>
			</Head>
			<form onSubmit={onSubmitLocal}>
			<div class="row">
				<div class="col-xl-4 col-md-12"></div>
				<div class="col-xl-4 col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="bt-wizard" id="besicwizard">
								<div class="tab-content">
									<div class="tab-pane show active" id="b-w-tab1">
									
											<div class="form-group row">
												<label for="b-t-name" class="col-sm-12 col-form-label">
													Imagen
												</label>
												<div class="col-sm-12">
													<input
														type="text"
														class="form-control"
														name="imageurl"
														onChange={handleChange}
														value={formState.values.imageurl ? items.imageurl : items.imageurl}
													/>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-name" class="col-sm-12 col-form-label">
													Categoria
												</label>
												<div class="col-sm-12">
													<input
														type="text"
														class="form-control"
														name="type"
														onChange={handleChange}
														value={formState.values.type ? items.type : items.type}
													/>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Status
												</label>
												<div class="col-sm-9">
													<select class="form-control"
														name="status"
														onChange={handleChange}>
														<option value={formState.values.status = "1"}>Activo</option>
														<option value={formState.values.status = "2"}>Inactivo</option>
													</select>
												</div>
											</div>
											<button type="submit" class="btn btn-primary btn-block">
												Guardar Cambios
											</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-12"></div>
		
			</div>
			</form>
		</Page>
	);
};

export default Edit;
