import { BASE_URL } from "./setting";

const ENDPOINT = `${BASE_URL}/api/users/details`;


export async function getUsers() {
  const token = localStorage.usertoken;
  const response = await fetch(`${ENDPOINT}`, {
    method: "GET",
    headers: {
        "Content-type": "application/json; charset=UTF-8",
        Accept: "application/json",
        Authorization: `Bearer ${token}`,
    },
  });

  const users = await response.json();
  return users;
}
