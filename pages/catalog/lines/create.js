import React, { useState } from "react";
import Notiflix from "notiflix";

const Create = () => {
	const [open, setOpen] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	const onSubmitAccount = (event) => {
		event.preventDefault();
		const token = localStorage.usertoken;

		fetch("http://localhost:3333/api/v1/admin/lines", {
			method: "POST",
			body: JSON.stringify({
				name: formState.values.name,
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then((response) => response.json())
			.then(function (response) {
				if (response[0]) {
					Notiflix.Notify.Failure(response[0].message);
				} else {
					Notiflix.Notify.Success("Línea Creada Exitosamente");
					const interval = setInterval(() => {
						window.location.reload(false);
					}, 2000);
					return () => clearInterval(interval);
				}
			});
	};

	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<div class="card-body">
			<form onSubmit={onSubmitAccount}>
				<div class="card-body">
					<div class="form-group">
						<label for="email2">Nombre</label>
						<input
							type="text"
							class="form-control"
							name="name"
							onChange={handleChange}
							value={formState.values.name}
							required
						/>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary btn-block">
							Guardar
						</button>
					</div>
				</div>
			</form>
		</div>
	);
};

export default Create;
