import React, { useState, useEffect } from "react";
import Notiflix from "notiflix";
import Head from "next/head";
import Page from "../../layout/main";
import fetch from "node-fetch";

const Create = ({ children }) => {
	const [locals, setLocals] = useState([]);
	const [users, setUsers] = useState([]);
	const [formState, setFormState] = useState({
		isValid: false,
		values: {},
		touched: {},
		errors: {},
	});

	const handleChange = (event) => {
		event.persist();

		setFormState((formState) => ({
			...formState,
			values: {
				...formState.values,
				[event.target.name]: event.target.value,
			},
		}));
	};

	useEffect(() => {
		const token = localStorage.usertoken;
		fetch("http://localhost:8000/api/details", {
			method: "GET",
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then(function (response) {
				return response.json();
			})
			.then(function (response) {
				setUsers(response.success);
				fetch("http://localhost:8000/api/local/list", {
					method: "POST",
					body: JSON.stringify({
						admin_id: response.success.id
					}),
					headers: {
						"Content-type": "application/json; charset=UTF-8",
						Accept: "application/json",
						Authorization: `Bearer ${token}`,
					},
				})
					.then(function (response) {
						return response.json();
					})
					.then(function (response) {
						if(response){
						setLocals(response);
						}else{
							setLocals([])
						}
					});
			});
	
	}, []);



	const onSubmitLocal = (event) => {
		event.preventDefault();
		const token = localStorage.usertoken;
	
		fetch("http://localhost:8000/api/collection/item", {
			method: "POST",
			body: JSON.stringify({
				admin_id: users.id,
				local_id: formState.values.local,
				type: formState.values.type,
				status: formState.values.status,
				imageurl: formState.values.imageurl
			}),
			headers: {
				"Content-type": "application/json; charset=UTF-8",
				Accept: "application/json",
				Authorization: `Bearer ${token}`,
			},
		})
			.then((response) => response.json())
			.then(function (response) {
				// if (response[0]) {
				// 	Notiflix.Notify.Failure(response[0].message);
				// } else {
				// 	Notiflix.Notify.Success("Marca Creada Exitosamente");
				// 	const interval = setInterval(() => {
				// 		window.location.reload(false);
				// 	}, 2000);
				// 	return () => clearInterval(interval);
				// }
			});
	};


	const notiflix = () => {
		Notiflix.Block.Standard(".loading", "Processing...");
		Notiflix.Block.Remove(".loading", 1000);
	};
	return (
		<Page>
			<Head>
				<title>Crear Colección</title>
			</Head>
			<form onSubmit={onSubmitLocal}>
			<div class="row">
				<div class="col-xl-4 col-md-12"></div>
				<div class="col-xl-4 col-md-12">
					<div class="card">
						<div class="card-body">
							<div class="bt-wizard" id="besicwizard">
								<div class="tab-content">
									<div class="tab-pane show active" id="b-w-tab1">
											<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-12 col-form-label">
													Local
												</label>
												<div class="col-sm-12">
													<select class="form-control"
														name="local"
														onChange={handleChange}>
															<option>Seleccione</option>
															{locals.map((locals, index) => (
																<option
																	key={index}
																	value={formState.values.local_id ? formState.values.local_id : locals.local_id}
																>
																	{locals.name}
																</option>
															))}
													</select>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-name" class="col-sm-12 col-form-label">
													Imagen
												</label>
												<div class="col-sm-12">
													<input
														type="text"
														class="form-control"
														placeholder="Ej: Comida"
														name="imageurl"
														onChange={handleChange}
														value={formState.values.imageurl}
													/>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-name" class="col-sm-12 col-form-label">
													Categoria
												</label>
												<div class="col-sm-12">
													<input
														type="text"
														class="form-control"
														placeholder="Ej: Comida"
														name="type"
														onChange={handleChange}
														value={formState.values.type}
													/>
												</div>
											</div>
											<div class="form-group row">
												<label for="b-t-pwd" class="col-sm-3 col-form-label">
													Status
												</label>
												<div class="col-sm-9">
													<select class="form-control"
														name="status"
														onChange={handleChange}>
														<option value={formState.values.status = "1"}>Activo</option>
														<option value={formState.values.status = "2"}>Inactivo</option>
													</select>
												</div>
											</div>
											<button type="submit" class="btn btn-primary btn-block">
												Crear Colección
											</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xl-4 col-md-12"></div>
		
			</div>
			</form>
		</Page>
	);
};

export default Create;
