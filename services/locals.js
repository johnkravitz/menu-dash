import { BASE_URL } from "./setting";

const ENDPOINT = `${BASE_URL}/api/local/item`;
const ENDPOINT_LIST = `${BASE_URL}/api/local/list`;
const ENDPOINT_LISTBY = `${BASE_URL}/api/local/listby`;
const ENDPOINT_DESTROY = `${BASE_URL}/api/local/destroy`;

export async function getLocals(id) {
  const token = localStorage.usertoken;

  const response = await fetch(ENDPOINT_LIST, {
    method: "POST",
    body: JSON.stringify({
      admin_id: id,
    }),
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
  });

  const locals = await response.json();
  return locals;
}

export async function addlocal(data) {
  const token = localStorage.usertoken;
  const alphabet=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

		let ranletter1 = alphabet[Math.floor(Math.random() * alphabet.length)];
		let ranletter2 = alphabet[Math.floor(Math.random() * alphabet.length)];
		let ranNum = Math.floor((Math.random() * 99999999999) * 7);
		let ranCode = ranletter1 + ranletter2 + ranNum;

  const res = await fetch(`${ENDPOINT}`, {
    method: "POST",
    body: JSON.stringify({
      admin_id: users.id,
      local_id: formState.values.local_id,
      tbl_local: ranCode,
      tbl_number: formState.values.tbl_number,
      status: formState.values.status
    }),
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    }
  });
  const local = await res.json();
  return local;
}

export async function editlocal(id, data) {
  const token = localStorage.usertoken;
  const alphabet=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];

  let ranletter1 = alphabet[Math.floor(Math.random() * alphabet.length)];
  let ranletter2 = alphabet[Math.floor(Math.random() * alphabet.length)];
  let ranNum = Math.floor((Math.random() * 99999999999) * 7);
  let ranCode = ranletter1 + ranletter2 + ranNum;

  const res = await fetch(`${ENDPOINT_UPDATE}/` + id, {
    method: "PATCH",
    body: JSON.stringify({
      admin_id: users.id,
      local_id: formState.values.local_id,
      tbl_local: ranCode,
      tbl_number: formState.values.tbl_number,
      status: formState.values.status
    }),
    headers: {
      "Content-Type": "application/json",
      Authorization: `Bearer ${token}`,
    }
  });
  const local = await res.json();
  return local;
}

export async function removelocal(id) {
  const token = localStorage.usertoken;

  const response = await fetch(`${ENDPOINT_DESTROY}`, {
    method: "DELETE",
    body: JSON.stringify({
      id: id
    }),
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json",
      Authorization: `Bearer ${token}`,
    },
  });

  const local = await response.json();
  return local;
}
