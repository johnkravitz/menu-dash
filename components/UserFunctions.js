import axios from "axios";

export const register = async (newUser) => {
	const response = await axios.post("http://localhost:5000/users/register", {
		first_name: newUser.first_name,
		last_name: newUser.last_name,
		email: newUser.email,
		password: newUser.password,
	});
	console.log("Registered");
};

export const login = async (user) => {
	try {
		const response = await axios.post(
			"http://localhost:8000/api/login",
			{
				email: user.email,
				password: user.password,
			}
		);
		localStorage.setItem("usertoken", response.data.success.token);
		console.log(localStorage, "localstorage");
		return response.data.success;
	} catch (err) {
		return "null";
	}
};